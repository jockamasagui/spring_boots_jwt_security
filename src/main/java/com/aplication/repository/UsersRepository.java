package com.aplication.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.aplication.configure.Users;

public interface UsersRepository extends JpaRepository<Users,Integer>{
  
	Users findByUsername(String username);
}
