package com.aplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aplication.configure.Users;
import com.aplication.repository.UsersRepository;

@RestController
public class UsersController {
	@Autowired
	UsersRepository usersRepository;
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public UsersController(BCryptPasswordEncoder bCryptPasswordEncoder ){
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@RequestMapping("/users")
	public @ResponseBody String getUsers(){
		return "{\"users\":[{\"firstname\":\"Richard\", \"lastname\":\"Feynman\"}," +
		           "{\"firstname\":\"Marie\",\"lastname\":\"Curie\"}]}";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/users")
	public void saveUsers(@RequestBody Users users){
		System.out.println(users.toString());
		users.setPassword(bCryptPasswordEncoder.encode(users.getPassword()));
		usersRepository.save(users);
	}
	
	

}
