package com.aplication.configure;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;

import com.aplication.repository.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import static java.util.Collections.emptyList;

import java.io.IOException;

public class TokenAuthenticationService {
      
    @Autowired
    UsersRepository usersRepository;
	
	  static final long EXPIRATIONTIME = 864_000_000; // 10 days
	  static final String SECRET = "ThisIsASecret";
	  static final String TOKEN_PREFIX = "Bearer";
	  static final String HEADER_STRING = "Authorization";
	  static final String USERNAME = "username";
	  
	  
	  static void addAuthentication(HttpServletResponse res, String username) throws IOException{
		  String JWT = Jwts.builder()
				  .setSubject(username)
				  .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				  .signWith(SignatureAlgorithm.HS512, SECRET)
				  .compact();
		  res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
		  res.addHeader(USERNAME, username);
		  //res.getWriter().write("{"+"username:"+username+"}");
		  //res.getWriter().flush();
		  //res.getWriter().close();
	  }
	  
	  public Users findUsers(String username){
		 return usersRepository.findByUsername(username);
	  }
	  
	  static Authentication getAuthentication(HttpServletRequest request) {
		  String token = request.getHeader(HEADER_STRING);
	        if (token != null) {
	            // parse the token.
	            String user = Jwts.parser()
	                    .setSigningKey(SECRET)
	                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
	                    .getBody()
	                    .getSubject();

	            return user != null ?
	                    new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
	                    null;
	        }
	        return null;
	    }
}
